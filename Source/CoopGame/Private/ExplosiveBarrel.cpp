// Creative Commons - Reconocimiento (by)

#include "../Public/ExplosiveBarrel.h"
#include "../Public/Components/SHealthComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Materials/MaterialInstanceDynamic.h"





// Sets default values
AExplosiveBarrel::AExplosiveBarrel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	MeshComp->SetSimulatePhysics(true);
	RootComponent = MeshComp;

	RadialForceComp = CreateDefaultSubobject<URadialForceComponent>(TEXT("URadialForceComp"));
}

// Called when the game starts or when spawned
void AExplosiveBarrel::BeginPlay()
{
	Super::BeginPlay();

	HealthComp->OnHealthChanged.AddDynamic(this, &AExplosiveBarrel::OnHealthChanged);

	auto Object = FindComponentByClass<UStaticMeshComponent>();
	auto Material = Object->GetMaterial(0);

	DynamicMaterial = UMaterialInstanceDynamic::Create(Material, NULL);
	Object->SetMaterial(0, DynamicMaterial);

	
}

// Called every frame
void AExplosiveBarrel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AExplosiveBarrel::OnHealthChanged(USHealthComponent * OwningHealthComp, float Health, float HealthDelta, const UDamageType * DamageType, AController * InstigatedBy, AActor * DamageCauser)
{
	if (Health <= 0.0f && !bUse)
	{
		// Die!
		bUse = true;
		UE_LOG(LogTemp, Log, TEXT("BOOM!!!"));
		UGameplayStatics::SpawnEmitterAtLocation(this, BoomFX, GetActorLocation());
		RadialForceComp->FireImpulse();
		MeshComp->AddImpulse(FVector(0, 0, 600), NAME_None, true);
		DynamicMaterial->SetVectorParameterValue("WallColor",FLinearColor::Black);
		DynamicMaterial->SetVectorParameterValue("FloorColor", FLinearColor::Black);
	}
}

